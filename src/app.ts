/// <reference path="../declarations/babylonjs.d.ts" />

import B from "babylonjs";

(()=>{
	if (B.Engine.isSupported()){
		let canvas = document.getElementById("model-viewer") as HTMLCanvasElement;
		let engine = new B.Engine(canvas, true);
		engine.enableOfflineSupport = false;

		B.SceneLoader.Load("./babylon/", "gundam-min.babylon", engine, (newScene):void => {
			newScene.executeWhenReady(()=>{
				newScene.meshes.map((mesh) => {
					if (mesh.name="turntable") {
						mesh.position = new BABYLON.Vector3(0, -13, 0);
					}
				});

				let skybox = BABYLON.Mesh.CreateBox("skyBox", 650, newScene);
				let skyboxMaterial = new BABYLON.StandardMaterial("skyBox", newScene);
				skyboxMaterial.backFaceCulling = false;
				skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("./babylon/skybox", newScene);
				skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
				skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
				skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
				skyboxMaterial.disableLighting = true;
				skybox.material = skyboxMaterial;

				let arcCamera = new BABYLON.ArcRotateCamera("ArcRotateCamera", 1, 0.8, 25, new BABYLON.Vector3(0, 0, 0), newScene);
				arcCamera.setPosition(new BABYLON.Vector3(0, 0, -40));
				arcCamera.target = new BABYLON.Vector3(0, 0, 0);

				newScene.activeCamera = arcCamera;
				arcCamera.attachControl(canvas, true);

				let light0 = new BABYLON.HemisphericLight("Hemi0", new BABYLON.Vector3(0, 30, 0), newScene);
				light0.diffuse = new BABYLON.Color3(1, 1, 1);
				light0.specular = new BABYLON.Color3(0.4, 0.4, 0.4);
				light0.groundColor = new BABYLON.Color3(0, 0, 0);


				newScene.registerBeforeRender(()=> {
					if (arcCamera.radius > 60) {
						arcCamera.radius = 60;
					} else if (arcCamera.radius < 5 ) {
						arcCamera.radius = 5;
					}
				});
				engine.runRenderLoop(()=>{
					newScene.render();
				})
			})
		}), function(progresss) {
			console.log(progresss);
		};
	}

})();