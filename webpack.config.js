let webpack = require('webpack');
let path = require("path");

let config = {
    entry: {
        main: "./src/app.ts"
    },
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist"
    },
    devtool: "source-map",

    plugins: [],

    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".js"],
        modules: [
            "node_modules"
        ]
    },

    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/, loader: 'babel-loader?presets[]=es2015!ts-loader'
            }
        ]
    },

    externals: {
        "babylonjs": "BABYLON"
    }
};


module.exports = function (env) {
    // if (env && env.hasOwnProperty("vendor")) {
    //     config.entry.vendor = [
    //
    //     ];
    //     config.plugins.push(new webpack.optimize.CommonsChunkPlugin({name: "vendor", filename: "vendor.bundle.js"}));
    // }

    if (env && env.hasOwnProperty("uglify")) {
        config.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                compress: {warnings: false},
                output: {comments: false},
                sourceMap: true
            })
        )
    }

    return config;
};